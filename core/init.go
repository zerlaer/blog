package core

import (
	"fmt"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"zerlaer.me/blog/config"
	"zerlaer.me/blog/global"
)

// InitConfig 读取yaml文件配置
func InitConfig() {
	const ConfigFile = "setting.yaml"
	c := &config.Config{}
	setting, err := ioutil.ReadFile(ConfigFile)
	if err != nil {
		fmt.Printf("错误: %s", err)
	}
	err = yaml.Unmarshal(setting, c)
	if err != nil {
		log.Fatalf("config init unmarshal: %v", err)
	}
	log.Println("配置文件初始化成功")
	global.Config = c
}
