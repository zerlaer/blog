package core

import (
	"fmt"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"log"
	"time"
	"zerlaer.me/blog/global"
)

func InitGorm() *gorm.DB {
	if global.Config.Mysql.Host == "" {
		log.Printf("未配置MySQL,取消连接")
		return nil
	}
	dsn := global.Config.Mysql.Dsn()
	var mysqlLogger logger.Interface
	if global.Config.System.Env == "dev" {
		mysqlLogger = logger.Default.LogMode(logger.Info)
	} else {
		mysqlLogger = logger.Default.LogMode(logger.Error)
	}
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{
		Logger: mysqlLogger,
	})
	if err != nil {
		log.Fatalf(fmt.Sprintf("[%s] MySQL 连接失败", dsn))
	}
	database, err := db.DB()
	database.SetMaxIdleConns(10)
	database.SetMaxOpenConns(100)
	database.SetConnMaxLifetime(time.Hour * 4)
	return db

}
