package main

import (
	"zerlaer.me/blog/core"
	"zerlaer.me/blog/global"
)

func main() {
	//读取配置文件
	core.InitConfig()
	// 初始化日志
	global.Log = core.InitLogger()
	global.Log.Info("Info")
	global.Log.Warn("Warn")
	global.Log.Error("Error")
	// 连接数据库
	global.DB = core.InitGorm()

}
